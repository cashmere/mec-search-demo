import styled from 'styled-components';

export const SearchFieldForm = styled.form`
  display: flex;
  margin: 20px auto;
  width: 50%;
`;

export const SearchFieldInput = styled.input`
  flex: 1 1 100%;
  border-radius: none;
  border: none;
  padding: 10px;
  font-size: 1rem;
  background: rgba(255, 255, 255, 0.1);
  color: white;
`;

export const SearchFieldButton = styled.button`
  background: rgba(0, 0, 0, 0.3);
  border: none;
  font-size: 20px;
  color: white;
  padding: 1rem;
  font-weight: bold;
`;
