import React, { useRef } from 'react';
import axios from 'axios';

import {
  SearchFieldForm,
  SearchFieldInput,
  SearchFieldButton
} from './styles';

const SearchField = ({ getResults, setLoading }) => {
  const searchField = useRef('');

  const handleGetResults = (event) => {
    event.preventDefault();
    setLoading(true);
    axios.get(`https://www.mec.ca/api/v1/products/search?keywords=${searchField.current.value}`)
      .then((res) => {
        setLoading(false);
        return getResults(res);
      })
      .catch((err) => {
        setLoading(false);
        return console.error(err)
      });
    }

  return (
    <SearchFieldForm onSubmit={evt => handleGetResults(evt)}>
      <SearchFieldInput type="text" ref={searchField} placeholder="Search for MEC products..." />
      <SearchFieldButton type="submit">Search</SearchFieldButton>
    </SearchFieldForm>
  );
};

export default SearchField;
