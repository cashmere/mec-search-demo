import React, { useState } from 'react';

import Logo from '../Logo';
import Loading from '../Loading';
import SearchField from '../SearchField';
import SearchResults from '../SearchResults';

function SearchPage() {
  const [results, getResults] = useState({ data: { products: [], total_product_count: 0 }, status: '' });
  const [loading, setLoading] = useState(false);

  return (
    <div>
      <header>
        <Logo />
        <SearchField 
          getResults={(results) => getResults(results)} 
          setLoading={(loading) => setLoading(loading)} />
      </header>
      {loading ?
        <Loading copy="Loading products..." /> :
        <SearchResults results={results.data.products} hasSearched={results.status === 200} totalResults={results.data.total_product_count} />
      }
    </div>
  );
}

export default SearchPage;
