import styled from 'styled-components';

export const ResultsInfo = styled.p`
  font-size: 0.75rem;
  color: rgba(255, 255, 255, 0.7);
  font-style: italic;
  margin: 30px auto 30px;
  text-align: center;
`;

export const ResultsWrap = styled.main`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  margin: auto;
`;

export const Result = styled.figure`
  flex: 1 1 20%;
  max-width: 300px;
  margin: 10px;
  position: relative;
  overflow: hidden;
  transition: box-shadow .2s ease-in-out;
  img {
    width: 100%;
  }
  figcaption {
    position: absolute;
    width: 100%;
    bottom: 0;
    left: 0;
    right: 0;
    background: #000;
    padding: 10px 20px 10px 10px;
    min-height: 10%;
  }
  p {
    color: white;
    font-size: 1rem;
    margin: 0;
  }
`;

export const NoResults = styled.div`
  display: ${props => props.show ? 'flex' : 'none'};
  color: white;
  text-align: center;
  margin: auto;
`;
