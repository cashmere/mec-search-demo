import React, { Fragment } from 'react';

import {
  ResultsInfo,
  ResultsWrap,
  Result,
  NoResults
} from './styles';

const SearchResults = ({ results = [], hasSearched = false, totalResults }) => {
  if (results.length === 0 && !hasSearched) return <Fragment />

  const renderAllResults = (
    <Fragment>
      <ResultsWrap>
        {results.map((result) => {
          return (
            <Result key={result.product_code}>
              <img src={result.default_image_urls.main_image_url} alt={result.full_name} />
              <figcaption>
                <p>{result.full_name}</p>
              </figcaption>
            </Result>
          );
        })}
      </ResultsWrap>
      <ResultsInfo>Showing {results.length} of {totalResults} products.</ResultsInfo>
    </Fragment>
  );

  const renderNoResults = <NoResults show={hasSearched}>No results.</NoResults>

  return results.length ? renderAllResults : renderNoResults;
}

export default SearchResults;
