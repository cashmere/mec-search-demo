import React from 'react';
import { render, screen } from '@testing-library/react';
import SearchResults from '../';

test('renders an empty fragment by default', () => {
  const props = {
    results: [],
    hasSearched: false,
    totalResults: 0
  }
  const { container } = render(<SearchResults {...props} />);
  expect(container).toBeEmpty();
})

test('renders no search results message', () => {
  const props = {
    results: [],
    hasSearched: true,
    totalResults: 0
  }
  render(<SearchResults {...props} />);
  expect(screen.getByText('No results.')).toBeInTheDocument();
});

test('renders 5 results and an info message', () => {
  const props = {
    results: [{
      product_code: 'aaa',
      default_image_urls: {
        main_image_url: 'http://placehold.it/233x233'
      },
      full_name: 'Demo product 1'
    },{
      product_code: 'bbb',
      default_image_urls: {
        main_image_url: 'http://placehold.it/233x233'
      },
      full_name: 'Demo product 2'
    },{
      product_code: 'ccc',
      default_image_urls: {
        main_image_url: 'http://placehold.it/233x233'
      },
      full_name: 'Demo product 3'
    },{
      product_code: 'ddd',
      default_image_urls: {
        main_image_url: 'http://placehold.it/233x233'
      },
      full_name: 'Demo product 4'
    },{
      product_code: 'eee',
      default_image_urls: {
        main_image_url: 'http://placehold.it/233x233'
      },
      full_name: 'Demo product 5'
    }],
    hasSearched: true,
    totalResults: 5
  }
  render(<SearchResults {...props} />);
  const items = screen.getAllByText(/Demo product [1-5]/)
  expect(items).toHaveLength(5);
  expect(screen.getByText('Showing 5 of 5 products.')).toBeInTheDocument();
});
