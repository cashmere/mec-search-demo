import styled from 'styled-components';

export const LogoWrap = styled.div`
  max-width: 125px;
  margin: 20px auto 0;
`;