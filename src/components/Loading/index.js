import React from 'react';

import { LoadingText } from './styles';

const Loading = ({ copy }) => {
  return (
    <LoadingText>{copy}</LoadingText>
  )
};

export default Loading;
