import styled from 'styled-components';

export const LoadingText = styled.p`
  color: white;
  text-align: center;
  margin: auto;
`;
